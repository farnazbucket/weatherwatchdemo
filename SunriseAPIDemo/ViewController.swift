//
//  ViewController.swift
//  SunriseAPIDemo
//
//  Created by MacStudent on 2019-10-16.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    @IBOutlet weak var resultsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickButtonPressed(_ sender: Any) {
        print("Button pressed")
        
        let URL = "http://api.weatherstack.com/current?access_key=2ac7c56185002df9692695ced1b217eb&query=toronto"
                       Alamofire.request(URL).responseJSON {
                       // 1. store the data from the internet in the
                       // response variable
                       response in
                           
                           // 2. get the data out of the variable
                           guard let apiData = response.result.value else {
                               print("Error getting data from the URL")
                               return
                           }
                           
                           // OUTPUT the entire json response to the terminal
                           print(apiData)
    
        }
    }
       
    
}

