//
//  CityInterfaceController.swift
//  SunriseAPIDemo WatchKit Extension
//
//  Created by Mahammad on 2019-11-04.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

class CitySettingsInterfaceController: WKInterfaceController {
    // MARK: Variables
     var city:String!
    
    // MARK: Default Functions
       // -----------------
       override func awake(withContext context: Any?) {
           super.awake(withContext: context)
           
           // Configure interface objects here.
       }
       
       override func willActivate() {
           // This method is called when watch view controller is about to be visible to user
           super.willActivate()
        
        let preferences = UserDefaults.standard
               guard let savedCity = preferences.string(forKey: "savedCity") else {
                   return
               }
       }
       
       override func didDeactivate() {
           // This method is called when watch view controller is no longer visible
           super.didDeactivate()
       }
       
    @IBOutlet weak var cityLabel: WKInterfaceLabel!
    
    
     // MARK: API KEYS
     let API_KEY = "2ac7c56185002df9692695ced1b217eb"
     var mainVC:InterfaceController?
    
    @IBAction func selectCity() {
    
    
    let suggestedResponses = ["Toronto", "Montreal","New York City","Los Angeles"]
           presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
               
               (results) in
               
               if (results != nil && results!.count > 0) {
                   // 2. write your code to process the person's response
                   let userResponse = results?.first as? String
                   self.cityLabel.setText(userResponse)
                   self.city = userResponse
               }
           }
    }
    
    @IBAction func saveButton() {
        print("Getting City")
              self.geocode(cityName: self.city)
    }
    func geocode(cityName:String) {
           
           // Get lat long using LocationIQ API (https://locationiq.com)
           // You can use Google, but you need to give Google your credit card number.
           // You can use the built in Apple CoreLocation library, but this library doesn't work properly with the emulator. See notes below.
           
           
           // TODO: Put your API call here
           
           // Encode the city name
           let cityParam = cityName.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
           
        let URL = "http://api.weatherstack.com/current?access_key=\(self.API_KEY)&q=\(cityParam!)&format=json"
        print(URL)
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            // Get the lat/long component out of the response url
            var jsonResponse = JSON(apiData)
            print(jsonResponse)
            let results = jsonResponse.array?.first
            
            if (results == nil) {
                print("Error parsing results from JSON response")
                return
            }
            
            let data = JSON(results)
          
            
            
            // save this to Shared Preferences
            let preferences = UserDefaults.standard
        
            preferences.set(self.city, forKey:"savedCity")
            
            // dismiss the View Controller
            self.popToRootController()
            
        }
    }
    
    
}
