//
//  InterfaceController.swift
//  SunriseAPIDemo WatchKit Extension
//
//  Created by MacStudent on 2019-10-16.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

class InterfaceController: WKInterfaceController {

    @IBOutlet weak var cityLabel: WKInterfaceLabel!
    @IBOutlet weak var timeLabel: WKInterfaceLabel!
    @IBOutlet weak var temperatureLabel: WKInterfaceLabel!
    
    
    
    override func awake(withContext context: Any?) {
           super.awake(withContext: context)
           
           // Configure interface objects here.
       }
       
       override func willActivate() {
           // This method is called when watch view controller is about to be visible to user
           super.willActivate()
           
           let preferences = UserDefaults.standard
        
         let URL = "http://api.weatherstack.com/current?access_key=2ac7c56185002df9692695ced1b217eb&query=toronto"
        
        print("Url: \(URL)")
        
                             Alamofire.request(URL).responseJSON {
                             // 1. store the data from the internet in the
                             // response variable
                             response in
                                 
                                 // 2. get the data out of the variable
                                 guard let apiData = response.result.value else {
                                     print("Error getting data from the URL")
                                     return
                                 }
                                 
                                 // OUTPUT the entire json response to the terminal
//                                 print(apiData)
                                
                                let jsonResponse = JSON(apiData)
                                let cityName = jsonResponse["location"]["name"].stringValue
                                print("City Name: \(cityName)")
                                let time = jsonResponse["location"]["localtime"].stringValue
                                print("Local Time: \(time)")
                                let temperature = jsonResponse["current"]["temperature"].stringValue
                                print("Local Time: \(temperature) degrees")
        }
    }
    override func didDeactivate() {
           // This method is called when watch view controller is no longer visible
           super.didDeactivate()
       }

    @IBAction func changeCity() {
        
    }
    
    
   
}
